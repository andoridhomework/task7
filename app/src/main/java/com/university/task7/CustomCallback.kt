package com.university.task7

interface CustomCallback {
    fun onSuccess(response: String) {}
    fun onFailed(errorMessage: String) {}
}
package com.university.task7

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.info_recyclerview_layout.view.*

object DataBinding {
    @JvmStatic
    @BindingAdapter("src")
    fun setImage(imageView: ImageView, image: String) {
        Glide.with(imageView.context)
            .load(image)
            .into(imageView.coverImageView)
    }
}
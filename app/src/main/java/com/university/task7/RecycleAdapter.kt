package com.university.task7

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.university.task7.databinding.InfoRecyclerviewLayoutBinding

class RecycleAdapter(private val infoo: ArrayList<Info>) :
    RecyclerView.Adapter<RecycleAdapter.ViewHolder>() {


    inner class ViewHolder(private val binding: InfoRecyclerviewLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind() {
            binding.info = infoo[adapterPosition]
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int
    ): ViewHolder {
        return ViewHolder(
            InfoRecyclerviewLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    }

    override fun getItemCount(): Int {
        return infoo.size
    }

    override fun onBindViewHolder(holder: RecycleAdapter.ViewHolder, position: Int) {
        holder.onBind()
    }
}